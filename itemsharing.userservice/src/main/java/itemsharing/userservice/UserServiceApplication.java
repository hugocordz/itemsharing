package itemsharing.userservice;

import itemsharing.userservice.model.Role;
import itemsharing.userservice.model.User;
import itemsharing.userservice.model.UserRole;
import itemsharing.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class UserServiceApplication implements CommandLineRunner{

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User();
        user1.setFirstName("Sergio");
        user1.setLastName("Contreras");
        user1.setUsername("scontreras");
        user1.setPassword("password");
        user1.setEmail("sergio.contreras@globant.com");


        Set<UserRole> userRoles = new HashSet<>();
        Role role1 = new Role();
        role1.setRoleId(1);
        role1.setName("ROLE_USER");
        userRoles.add(new UserRole(user1, role1));

        userService.createUser(user1);

    }
}
