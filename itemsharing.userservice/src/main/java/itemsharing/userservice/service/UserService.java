package itemsharing.userservice.service;

import itemsharing.userservice.model.User;


public interface UserService {

    User createUser(User user);
    User getUserByUsername(String username);
}
