package com.itemsharing.itemservice;

import com.itemsharing.itemservice.model.Item;
import com.itemsharing.itemservice.model.User;
import com.itemsharing.itemservice.service.ItemService;
import com.itemsharing.itemservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import java.util.Date;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class ItemserviceApplication implements CommandLineRunner{

    @Autowired
    private ItemService itemService;

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(ItemserviceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = userService.findByUsername("scontreras");

        Item item = new Item();
        item.setName("Item1");
        item.setItemCondition("New");
        item.setStatus("Active");
        item.setAddDate(new Date());
        item.setDescription("This is an item description");
        item.setUser(user);

        itemService.addItemByUser(item, user.getUsername());

        Item item2 = new Item();
        item2.setName("Item2");
        item2.setItemCondition("Used");
        item2.setStatus("Inactive");
        item2.setAddDate(new Date());
        item2.setDescription("This is an item description for item 2");
        item2.setUser(user);

        itemService.addItemByUser(item2, user.getUsername());
    }
}
